#!/usr/bin/env python3
import boto3
import logging
import json
import os
import sys
import datetime
from dateutil.relativedelta import relativedelta
from dateutil.tz import tzutc
from pprint import pformat, pprint


loglevel = os.environ['LogLevel'] if 'LogLevel' in os.environ else logging.INFO
logger = logging.getLogger()
logger.setLevel(loglevel)
logging.getLogger('boto3').setLevel(logging.WARNING)
logging.getLogger('botocore').setLevel(logging.WARNING)


default_daily_retention = os.getenv('DAILY_RETENTION', 14)
default_monthly_retention = os.getenv('MONTHLY_RETENTION', 6)


ec2 = boto3.client('ec2')


def get_backup_enabled_instances():
    all_instances = dict()
    not_done = True
    token = None
    volumes = 0
    logger.info("Getting all instances to be backed up...")
    while not_done:
        if token is not None:
            describe_instances = ec2.describe_instances(Filters=[{'Name': 'tag:backup:enabled', 'Values': ['yes', 'true', '1']}], NextToken=token, MaxResults=100)
        else:
            describe_instances = ec2.describe_instances(Filters=[{'Name': 'tag:backup:enabled', 'Values': ['yes', 'true', '1']}], MaxResults=100)
        token = (describe_instances['NextToken'] if 'NextToken' in describe_instances else None)
        if token is None:
            logger.debug("No NextToken found..")
            not_done = False
        for reservation in describe_instances['Reservations']:
            for instance in reservation['Instances']:
                logger.debug(instance)
                instance_tags = dict()
                for tag in instance['Tags']:
                    instance_tags[tag['Key']] = tag['Value']
                daily_retention = (instance_tags['backup:daily-retention'] if 'backup:daily-retention' in instance_tags else default_daily_retention)
                monthly_retention = (instance_tags['backup:monthly-retention'] if 'backup:monthly-retention' in instance_tags else default_monthly_retention)
                all_instances[instance['InstanceId']] = dict()
                all_instances[instance['InstanceId']]['volumes'] = list()
                for volume in instance['BlockDeviceMappings']:
                    volume_dict = dict()
                    volume_dict['volume-id'] = volume['Ebs']['VolumeId']
                    volume_dict['volume-name'] = volume['DeviceName']
                    volume_dict['attach-time'] = volume['Ebs']['AttachTime'].strftime("%Y-%m-%d %H:%M:%S")
                    all_instances[instance['InstanceId']]['volumes'].append(volume_dict)
                    volumes += 1
                all_instances[instance['InstanceId']]['daily-retention'] = daily_retention
                all_instances[instance['InstanceId']]['monthly-retention'] = monthly_retention
    logger.info("Found {} instances with a total of {} volumes".format(len(all_instances), volumes))
    return all_instances


def create_snapshots(instances):
    snapshots = dict()
    count = 0

    if datetime.datetime.now().day is 1:
        snapshot_type = 'monthly'
    else:
        snapshot_type = 'daily'

    logger.info("Creating snapshots for all enabled instances...")
    for key in instances:
        for volume in instances[key]['volumes']:
            snapshot = ec2.create_snapshot(Description='Automated Snapshot via Lambda',
                                           VolumeId=volume['volume-id'],
                                           TagSpecifications=[{
                                               'ResourceType': 'snapshot',
                                               'Tags': [
                                                   {
                                                       'Key': 'Name',
                                                       'Value': "{}:{}:{}".format(snapshot_type, key, volume['volume-id'])
                                                   },
                                                   {
                                                       'Key': 'instance-id',
                                                       'Value': key
                                                   },
                                                   {
                                                       'Key': 'volume-id',
                                                       'Value': volume['volume-id']
                                                   },
                                                   {
                                                       'Key': 'volume-name',
                                                       'Value': volume['volume-name']
                                                   },
                                                   {
                                                       'Key': 'attach-time',
                                                       'Value': volume['attach-time']
                                                   },
                                                   {
                                                       'Key': 'snapshot-type',
                                                       'Value': snapshot_type
                                                   },
                                                   {
                                                       'Key': 'retention',
                                                       'Value': str(instances[key]["{}-retention".format(snapshot_type)])
                                                   }
                                               ]
                                           }])
            logger.debug(snapshot)
            logger.info("Created snapshot {} of volume {}".format(snapshot['SnapshotId'], volume['volume-id']))
            count += 1
    logger.info("Created {} snapshots".format(count))


def get_snapshots():
    all_snapshots = dict()
    not_done = True
    token = None
    logger.info("Getting all snapshots created by this script...")
    while not_done:
        if token is not None:
            describe_snapshots = ec2.describe_snapshots(Filters=[{'Name': 'description', 'Values': ['Automated Snapshot via Lambda']}], NextToken=token, MaxResults=100)
        else:
            describe_snapshots = ec2.describe_snapshots(Filters=[{'Name': 'description', 'Values': ['Automated Snapshot via Lambda']}], MaxResults=100)
        token = (describe_snapshots['NextToken'] if 'NextToken' in describe_snapshots else None)
        if token is None:
            logger.debug("No NextToken found..")
            not_done = False
        for snapshot in describe_snapshots['Snapshots']:
            logger.debug(snapshot)
            snapshot_tags = dict()
            for tag in snapshot['Tags']:
                snapshot_tags[tag['Key']] = tag['Value']
            all_snapshots[snapshot['SnapshotId']] = dict()
            all_snapshots[snapshot['SnapshotId']]['tags'] = snapshot_tags
            all_snapshots[snapshot['SnapshotId']]['start-time'] = snapshot['StartTime']
    logger.info("Found {} snapshots".format(len(all_snapshots)))
    return all_snapshots


def cleanup_old_snapshots(snapshots):
    current_time = datetime.datetime.now(tz=tzutc())

    logger.info("Deleting snapshots which are past the retention period...")
    count = 0
    for key in snapshots:
        if snapshots[key]['tags']['snapshot-type'] == 'daily':
            deletion_time = snapshots[key]['start-time'] + relativedelta(days=int(snapshots[key]['tags']['retention']))
        elif snapshots[key]['tags']['snapshot-type'] == 'monthly':
            deletion_time = snapshots[key]['start-time'] + relativedelta(days=int(snapshots[key]['tags']['retention']))
        if current_time > deletion_time:
            logger.info("Deleting snapshot {} because its past the retention period".format(key))
            delete = ec2.delete_snapshot(SnapshotId=key)
            count += 1
            logger.debug(delete)
    logger.info("Deleted {} old snapshots".format(count))


def lambda_handler(event, context):
    logger.info("Lambda received the event %s", pformat(event))
    logger.info("Starting...")
    instances = get_backup_enabled_instances()
    snapshots = create_snapshots(instances)
    snaps = get_snapshots()
    cleanup_old_snapshots(snaps)
    logger.info("Done!")


if __name__ == "__main__":
    instances = get_backup_enabled_instances()
    snapshots = create_snapshots(instances)
    snaps = get_snapshots()
    cleanup_old_snapshots(snaps)
